ActiveAdmin.register Lama do
  permit_params :title, :description, :discount_value, :price, :cover,
                :remain_count, :category_id, :date_from, :date_to

  index do
    id_column
    column :title
    column :description
    column :discount_value
    column :price
    column :remain_count
    column :category_id
    column :date_from
    column :date_to

    actions
  end

  form(html: { multipart: true }) do |f|
    f.inputs 'Lama' do
      f.input :title
      f.input :description
      f.input :discount_value
      f.input :price
      f.input :remain_count
      f.input :category
      f.input :date_from
      f.input :date_to
      f.input :cover, as: :file
    end
    actions
  end
end
