ActiveAdmin.register Category do
  permit_params :name


  index do
    id_column
    column :name
    actions
  end

end
