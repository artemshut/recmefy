class Lama < ApplicationRecord
  mount_uploader :cover, CoverUploader

  searchable do
    text :title, :description

    integer :category_id
  end
  # extend FriendlyId
  # friendly_id :title, use: [:slugged, :finders]

  belongs_to :category
end