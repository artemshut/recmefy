class LamaDecorator < Draper::Decorator
  delegate_all

  def signed_discount_value
    "#{discount_value}%"
  end

  def price
    "#{object.price}р"
  end

  def savings
    "#{(object.price.to_i / (discount_value * 100).to_f).round(2)} р"
  end
end