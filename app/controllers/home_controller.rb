class HomeController < ApplicationController
  def index
    @lamas = Lama.all.decorate
    @max_dicsount = Lama.maximum(:discount_value)
  end
end