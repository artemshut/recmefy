class LamasController < ApplicationController
  def search
    @lamas = Lama.search do
      fulltext params[:q]

      with :category_id, params[:category_id]
    end.result
  end
end