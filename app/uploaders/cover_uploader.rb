class CoverUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick

  version :small_lama do
    process resize_to_fill: [700, 420]
  end

  storage :file

  def extension_white_list
    %w(jpg jpeg gif png)
  end
end