Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_for :users, controllers: { registrations: 'users/registrations' }

  resources :lamas do
    collection do
      get 'search'
    end
  end

  get 'home/index'
  root 'home#index'

end
