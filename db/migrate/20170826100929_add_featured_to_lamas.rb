class AddFeaturedToLamas < ActiveRecord::Migration[5.0]
  def change
    add_column :lamas, :featured, :boolean, default: false
  end
end
