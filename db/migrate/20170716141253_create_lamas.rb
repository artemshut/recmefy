class CreateLamas < ActiveRecord::Migration[5.0]
  def change
    create_table :lamas do |t|
      t.string :title
      t.text :description
      t.integer :discount_value
      t.string :price
      t.integer :remain_count
      t.integer :category_id
      t.datetime :date_from
      t.datetime :date_to

      t.timestamps
    end
  end
end
