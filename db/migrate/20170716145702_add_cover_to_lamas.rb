class AddCoverToLamas < ActiveRecord::Migration[5.0]
  def change
    add_column :lamas, :cover, :string
  end
end
