class AddSlugToLama < ActiveRecord::Migration[5.0]
  def change
    add_column :lamas, :slug, :string, uniq: true
  end
end
