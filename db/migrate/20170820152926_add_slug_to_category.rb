class AddSlugToCategory < ActiveRecord::Migration[5.0]
  def change
    add_column :categories, :slug, :string, uniq: true
  end
end
